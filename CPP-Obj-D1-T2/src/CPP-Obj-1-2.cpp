//============================================================================
// Name        : CPP-Obj-1-2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "headers/Fighter.h"
#include "headers/Triangle.h"
#include "headers/Rectangle.h"
#include "headers/Mother.h"
#include "headers/Zebra.h"
#include "headers/Dolphin.h"
#include "headers/Doughter.h"

void battle(Fighter& aFigther1, Fighter& aFighter2)
{
	bool isTurnOfFither1 = 1;
	do
	{
		if (isTurnOfFither1 == 1)
		{
			aFighter2.setHealth(aFighter2.getHealth() - aFigther1.getDamageAttack());
			isTurnOfFither1 = false;
		}
		else
		{
			aFigther1.setHealth(aFigther1.getHealth() - aFighter2.getDamageAttack());
			isTurnOfFither1 = true;
		}
	}
	while (aFigther1.getHealth() > 0 || aFighter2.getHealth() > 0);

	if (aFigther1.getHealth() > 0)
	{
		std::cout << aFigther1.getName() << " has won the battle." << std::endl;
	}
	else
	{
		std::cout << aFighter2.getName() << " has won the battle." << std::endl;
	}
}

int main() {

	// Task 1,2,3 //////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	Fighter f1("Tank", 20, 1), f2("Rogue", 8, 5);
	battle(f1, f2);

	// Task 4 //////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	Triangle t1(1, 2);
	Rectangle r1(3, 2);
	std::cout << t1.area() << std::endl;
	std::cout << r1.area() << std::endl;

	// Task 5 //////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	//Mother::Doughter d1;	-> old solution when Doughter class was implemented inside Mother class
	Doughter d1;
	d1.display();

	// Task 6 //////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	Zebra z1;
	Dolphin del1;
	z1.set_value("Pasiak", 3);
	del1.set_value("Nurek", 2);
	z1.showInfo();
	std::cout << std::endl;
	del1.showInfo();

	return 0;
}
