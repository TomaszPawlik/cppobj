/*
 * Doughter.cpp
 *
 *  Created on: Mar 20, 2019
 *      Author: tpawlik
 */

#include "headers/Doughter.h"

void Doughter::display()
{
	std::cout << "Doughter class display method." << std::endl;
}
