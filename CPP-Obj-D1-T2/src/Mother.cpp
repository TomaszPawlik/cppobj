/*
 * Mother.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Mother.h"

void Mother::display()
{
	std::cout << "Mother class display method." << std::endl;
}
