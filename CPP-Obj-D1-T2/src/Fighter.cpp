/*
 * Fighter.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Fighter.h"

Fighter::Fighter(std::string aName, int aHealth, int aDmg) :
mName(aName),
mHealth(aHealth),
mDamageAttack(aDmg)
{

};

void Fighter::setName(std::string aValue)
{
	mName = aValue;
};

std::string Fighter::getName()
{
	return mName;
};
void Fighter::setHealth(int aValue)
{
	mHealth = aValue;
};

int Fighter::getHealth()
{
	return mHealth;
};

void Fighter::setDamageAttack(int aValue)
{
	mDamageAttack = aValue;
};

int Fighter::getDamageAttack()
{
	return mDamageAttack;
};
