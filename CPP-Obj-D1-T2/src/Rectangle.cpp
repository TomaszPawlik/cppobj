/*
 * Rectangle.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */


#include "headers/Rectangle.h"

Rectangle::Rectangle(double aWidth, double aHeight) : Shape(aWidth,aHeight)
{

}

double Rectangle::area()
{
	return mWidth*mHeight;
}

