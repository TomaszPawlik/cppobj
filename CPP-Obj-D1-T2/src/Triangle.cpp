/*
 * Triangle.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Triangle.h"

Triangle::Triangle(double aWidth, double aHeight) : Shape(aWidth,aHeight)
{

}

double Triangle::area()
{
	return mWidth*mHeight/2;
}



