/*
 * Shape.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_SHAPE_H_
#define HEADERS_SHAPE_H_

class Shape
{
public:
	Shape(double aWidth, double aHeight);
protected:
	double mWidth;
	double mHeight;
};

#endif /* HEADERS_SHAPE_H_ */
