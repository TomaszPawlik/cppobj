/*
 * Fighter.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_FIGHTER_H_
#define HEADERS_FIGHTER_H_

#include <string>

class Fighter
{
public:
	Fighter(std::string aName, int aHealth, int aDmg);
	void setName(std::string aValue);
	std::string getName();
	void setHealth(int aValue);
	int getHealth();
	void setDamageAttack(int aValue);
	int getDamageAttack();
private:
	std::string mName;
	int mHealth;
	int mDamageAttack;
};



#endif /* HEADERS_FIGHTER_H_ */
