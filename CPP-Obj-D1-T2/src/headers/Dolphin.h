/*
 * Dolphin.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_DOLPHIN_H_
#define HEADERS_DOLPHIN_H_

#include <iostream>
#include "Animal.h"

class Dolphin : public Animal
{
public:
	void showInfo();
};



#endif /* HEADERS_DOLPHIN_H_ */
