/*
 * Mother.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_MOTHER_H_
#define HEADERS_MOTHER_H_

#include <iostream>

class Mother
{
public:
	void display();
};

#endif /* HEADERS_MOTHER_H_ */
