/*
 * Zebra.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_ZEBRA_H_
#define HEADERS_ZEBRA_H_

#include <iostream>
#include "Animal.h"

class Zebra : public Animal
{
public:
	void showInfo();
};



#endif /* HEADERS_ZEBRA_H_ */
