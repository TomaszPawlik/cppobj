/*
 * Animal.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_ANIMAL_H_
#define HEADERS_ANIMAL_H_

#include <string>

class Animal
{
public:
	void set_value(std::string aName, int aAge);
protected:
	std::string mName;
	int mAge;
};



#endif /* HEADERS_ANIMAL_H_ */
