/*
 * Doughter.h
 *
 *  Created on: Mar 20, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_DOUGHTER_H_
#define HEADERS_DOUGHTER_H_

#include "Mother.h"

class Doughter : public Mother
{
public:
	void display();
};

#endif /* HEADERS_DOUGHTER_H_ */
