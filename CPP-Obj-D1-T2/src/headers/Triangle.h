/*
 * Triangle.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */



#ifndef HEADERS_TRIANGLE_H_
#define HEADERS_TRIANGLE_H_

#include "Shape.h"

class Triangle : public Shape
{
public:
	Triangle(double aWidth, double aHeight);
	double area();
};



#endif /* HEADERS_TRIANGLE_H_ */
