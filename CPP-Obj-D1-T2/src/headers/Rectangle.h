/*
 * Rectangle.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */


#ifndef HEADERS_RECTANGLE_H_
#define HEADERS_RECTANGLE_H_

#include "Shape.h"

class Rectangle : public Shape
{
public:
	Rectangle(double aWidth, double aHeight);
	double area();
};



#endif /* HEADERS_TRIANGLE_H_ */
