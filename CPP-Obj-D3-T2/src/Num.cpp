/*
 * Num.cpp
 *
 *  Created on: Mar 21, 2019
 *      Author: tpawlik
 */

#include "headers/Num.h"

Num::Num(int aN) : mN(aN)
{
}

Num& Num::operator++()
{
	++mN;
	return *this;
}
