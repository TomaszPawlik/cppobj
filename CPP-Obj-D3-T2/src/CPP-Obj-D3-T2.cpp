//============================================================================
// Name        : CPP-Obj-D3-T2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <limits>
#include <stdint.h>
#include "headers/MyTempClass.h"
#include "headers/MyIncrementerStatic.h"
#include "headers/Num.h"
/*
 * Day 3 Task 2
 *
    Create template function, which will substract two numbers.
    Try it with various combinations of types int, uint, double, short and with numbers which does not fit into other types (like 100000 does not fit to short).
    What type should be returned? Is it possible to cover all those cases with the same template? Add text file to commit with console output which shows your experiments.

    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    Create a template class which will have function increment(T number) to increment private member T value;
    Add public function to get current value of `value` member, print it to the console.
    Try this function for different types.
    ** Define simple class Num which will have only single public member n.
    Is it possible to instantiate your template for class Num?
    Add text file to commit with console output which shows your experiments.
 */

template <class T1>
T1 subtractionV1(T1 aVal1, T1 aVal2)
{
	return aVal1 - aVal2;
}

template <class T1, class T2>
T1 subtractionV2(T1 aVal1, T2 aVal2)
{
	return aVal1 - aVal2;
}

int main()
{
	int intVal1 = 100000;
	unsigned int uIntVal1 = 55555;
	double doubleVal1 = 5.4673;
	short shortVal1 = 1;
	//short newShort = subtractionOneTemp(shortVal1, 50);
	//short newShort = subtractionOneTemp(shortVal1, static_cast<short>(50));
	short newShort = subtractionV2(shortVal1, 50);
	std::cout << newShort << std::endl;

	// //////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	MyTempClass<uint32_t> tempCObj1(80);
	MyTempClass<double> tempCObj2(20.0);

	std::cout << "Temp class for int type: " << tempCObj1.getValue() << std::endl;
	tempCObj1.increment();
	std::cout << "Temp class for int type, after increment: " << tempCObj1.getValue() << std::endl;

	std::cout << "Temp class for double type: " << tempCObj2.getValue() << std::endl;
	tempCObj2.increment();
	std::cout << "Temp class for double type, after increment: " << tempCObj2.getValue() << std::endl;

	MyTempClass<Num> tempCObj3(5);
	tempCObj3.increment();
	std::cout << "Inc for Num class (with preincrement operator overload): " << tempCObj3.getValue().mN << std::endl;

	return 0;
}
