/*
 * MyIncrementer.h
 *
 *  Created on: Mar 20, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_MYINCREMENTERSTATIC_H_
#define HEADERS_MYINCREMENTERSTATIC_H_

/////////////////////////////////////////////////////////////////////////////////////////
//This part is not related to task - I just wanted to do something with static method.

template <class T>
class MyIncrementerStatic
{
public:
	static void increment(T& aNumber)
	{
		aNumber++;
	}
};

/////////////////////////////////////////////////////////////////////////////////////////

#endif /* HEADERS_MYINCREMENTERSTATIC_H_ */
