/*
 * Num.h
 *
 *  Created on: Mar 21, 2019
 *      Author: tpawlik
 */

#ifndef NUM_H_
#define NUM_H_

class Num
{
public:
	Num() = delete;
	Num(int aN);
	~Num() = default;
	Num& operator++();
	int mN;
};

#endif /* NUM_H_ */
