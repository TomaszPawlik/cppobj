/*
 * MyTempClass.h
 *
 *  Created on: Mar 20, 2019
 *      Author: tpawlik
 */

#ifndef MYTEMPCLASS_H_
#define MYTEMPCLASS_H_

template <class T>
class MyTempClass {
public:
	MyTempClass() = delete;
	MyTempClass(T aValue) : mValue(aValue){};
	virtual ~MyTempClass() = default;

	T& getValue()
	{
		return mValue;
	}

	void increment()
	{
		++mValue;
	}

private:
	T mValue;
};

#endif /* MYTEMPCLASS_H_ */
