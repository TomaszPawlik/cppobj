/*
 * Derived.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Derived.h"
#include <iostream>

Derived::Derived()
{
	std::cout << "Derived const : " << this << std::endl;

}

Derived::~Derived()
{
	std::cout << "Derived destr : " << this << std::endl;
}

