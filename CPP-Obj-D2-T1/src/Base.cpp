/*
 * Base.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Base.h"
#include <iostream>

Base::Base()
{
	std::cout << "Base const : " << this << std::endl;

}

Base::~Base() {
	std::cout << "Base destr : " << this << std::endl;
}
