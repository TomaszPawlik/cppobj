/*
 * Neva.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/NevaCat.h"
#include <iostream>

void NevaCat::meow() const
{
	std::cout << "NevaCat meow." << std::endl;
}
