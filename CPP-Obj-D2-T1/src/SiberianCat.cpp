/*
 * SiberianCat.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/SiberianCat.h"
#include <iostream>

void SiberianCat::meow() const
{
	std::cout << "SiberianCat meow." << std::endl;
}
