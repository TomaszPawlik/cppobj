//============================================================================
// Name        : CPP-Obj-D2-T1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


//Create class which represent a cat (in general) then two deriving classes to represent Siberian and Neva cats. Make Cat pure virtual.
//Create classes: Base and Derived. Define ctors and dtors for both, that derived object is created/destructed correctly (what order of ctors and dtors calling is desired?)



#include <iostream>
#include "headers/Cat.h"
#include "headers/NevaCat.h"
#include "headers/SiberianCat.h"
#include "headers/Derived.h"

int main()
{

	Cat * catPtr;

	catPtr = new SiberianCat();
	catPtr->meow();
	delete catPtr;

	catPtr = new NevaCat();
	catPtr->meow();
	delete catPtr;

	Derived d1;				// Variable used to display sequence of const / destructors

	return 0;
}
