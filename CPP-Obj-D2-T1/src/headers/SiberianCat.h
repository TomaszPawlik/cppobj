/*
 * SiberianCat.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef SIBERIANCAT_H_
#define SIBERIANCAT_H_

#include "Cat.h"

class SiberianCat: public Cat
{
public:
	SiberianCat() = default;
	virtual ~SiberianCat() = default;
	void meow() const override;
};

#endif /* SIBERIANCAT_H_ */
