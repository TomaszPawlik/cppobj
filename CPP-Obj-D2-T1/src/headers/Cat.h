/*
 * Cat.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef CAT_H_
#define CAT_H_


class Cat
{
public:
	Cat() = default;
	virtual ~Cat() = default;
	virtual void meow() const = 0;
};

#endif /* CAT_H_ */
