/*
 * Base.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef BASE_H_
#define BASE_H_

class Base
{
public:
	Base();
	virtual ~Base();
};

#endif /* BASE_H_ */
