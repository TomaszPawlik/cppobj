/*
 * Derived.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef DERIVED_H_
#define DERIVED_H_

#include "Base.h"

class Derived: public Base {
public:
	Derived();
	virtual ~Derived();
};

#endif /* DERIVED_H_ */
