/*
 * Neva.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef NEVA_H_
#define NEVA_H_

#include "Cat.h"

class NevaCat: public Cat
{
public:
	NevaCat() = default;
	virtual ~NevaCat() = default;
	void meow() const override;
};

#endif /* NEVA_H_ */
