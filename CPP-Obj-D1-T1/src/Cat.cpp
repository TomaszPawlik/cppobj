/*
 * Cat.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Cat.h"
#include <iostream>
#include <stdint.h>

Cat::Cat(std::string aName)
: mName(aName)
{
};

void Cat::meow(uint8_t howLoud)
{
	std::string display = "Task8: meow";

	for (uint8_t count = 0; count < howLoud; ++count)
	{
		display.append("!");
	}

	std::cout << display << std::endl;;
};

void Cat::setName(std::string aName)
{
	mName = aName;
};

std::string Cat::getName()
{
	return mName;
};
