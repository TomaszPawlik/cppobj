//============================================================================
// Name        : CPP.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "headers/Task1.h"
#include "headers/Task2.h"
#include "headers/Task3.h"
#include "headers/Cat.h"
using namespace std;

class EmptyClassTrial{

};

int main() {

	EmptyClassTrial ect();

	// Task 1 //////////////////////////////////////5,1///////////
	//////////////////////////////////////////////////////////////

	Task1 t1;
	std::cout << "Task1: " << t1.countTriangleArea() << std::endl;

	// Task 2 //////////////////////////////////////////////////
	////////////////////////////////////////////////////////////

	int value1, value2;
	std::cout << "Task2: Podaj pierwsza wartosc int: ";
	cin >> value1;
	std::cout << "Task2: Podaj druga wartosc int: ";
	cin >> value2;

	Task2 t2(value1,value2);
	std::cout << "Task2: Wynik dodawania: "<< t2.addition() << std::endl;

	// Task 3 //////////////////////////////////////////////////
	////////////////////////////////////////////////////////////

	//Perform addition operation on complex data using class and object.
	//The program should ask for real and imaginary part of two complex numbers, and display the real and imaginary parts of their sum.

	Task3 t3;
	t3.getData();

	// Task 4,5,6,7,8 //////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	Cat c1("Krakers");
	c1.meow(3);

	return 0;
}
