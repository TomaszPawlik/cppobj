/*
 * Task3.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Task3.h"

void Task3::displayData()
{
	std::cout << "Real 1: " << valueReal1 << std::endl;
	std::cout << "Imaginary 1: " << valueImaginary1 << std::endl;
	std::cout << "Real 2: " << valueReal2 << std::endl;
	std::cout << "Imaginary 1: " << valueImaginary1 << std::endl;
}

void Task3::getData()
{
	std::cout << "Task3: Podaj wartosc real dla pierwszej liczby : ";
	std::cin >> valueReal1;
	std::cout << "Task3: Podaj wartosc imaginary dla pierwszej liczby : ";
	std::cin >> valueImaginary1;
	std::cout << "Task3: Podaj wartosc real dla drugiej liczby : ";
	std::cin >> valueReal2;
	std::cout << "Task3: Podaj wartosc imaginary dla drugiej liczby : ";
	std::cin >> valueImaginary2;

	displayData();
};

