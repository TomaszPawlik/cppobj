/*
 * Task2.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_TASK2_H_
#define HEADERS_TASK2_H_

class Task2{
public:
	Task2(int aValue1, int aValue2);
	int addition();
private:
	int mIntValue1;
	int mIntValue2;
};

#endif /* HEADERS_TASK2_H_ */
