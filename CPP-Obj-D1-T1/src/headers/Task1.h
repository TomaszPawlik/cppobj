/*
 * Task1.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_TASK1_H_
#define HEADERS_TASK1_H_

class Task1{
public:
	double countTriangleArea();
private:
	double mTriangleBase=0;
	double mTriangleHeight=0;
};

#endif /* HEADERS_TASK1_H_ */
