/*
 * Cat.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_CAT_H_
#define HEADERS_CAT_H_

#include <stdint.h>
#include <string>

class Cat
{
public:
	Cat(std::string aName);
	void meow(uint8_t howLoud);
	void setName(std::string aName);
	std::string getName();
private:
	uint8_t weight;
	std::string mName;
};

#endif /* HEADERS_CAT_H_ */
