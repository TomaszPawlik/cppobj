/*
 * Task3.h
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#ifndef HEADERS_TASK3_H_
#define HEADERS_TASK3_H_

#include <iostream>

class Task3{
public:
	void getData();
private:
	double valueReal1, valueReal2, valueImaginary1, valueImaginary2;
	void displayData();
};

#endif /* HEADERS_TASK3_H_ */
