/*
 * Task2.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: tpawlik
 */

#include "headers/Task2.h";

Task2::Task2(int aValue1, int aValue2)
: mIntValue1(aValue1),
  mIntValue2(aValue2)
{

};

int Task2::addition()
{
	return mIntValue1+mIntValue2;
};
