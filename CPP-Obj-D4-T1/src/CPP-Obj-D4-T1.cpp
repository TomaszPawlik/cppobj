//============================================================================
// Name        : CPP-Obj-D4-T1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/*
 * Day 4

 *	Task 1

    Create vector of int. Create an expression which will:
    (1)    return number of odd elements
    (2)    return sum of elements which are less than 10

 *	Task 2

    Create vector of std::string. Create an expression which will:
    (3)    return concatenated elements
    (4)    return number of elements, which size is greater than 3 characters
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <numeric>

std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

int main() {

	// Task 1 ///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<int> intVec {64, 5, 6, 4, 3, 6, 4, 64, 32, 5, 78, 1, 2, 47, 3};

	/*1*/
	auto isOdd = [] (int intVal) { return ((intVal % 2) == 1); };
	auto countOdd = std::count_if (intVec.begin(), intVec.end(), isOdd);
	std::cout << "Number of odd elements in vector: " << countOdd << std::endl;

	/*2*/
	auto sumLessThanTen = std::accumulate(intVec.begin(), intVec.end(), 0, [](int totalsum, int indexVal)
		{
			if (indexVal < 10)
				return totalsum+indexVal;
			return totalsum;
		});
	std::cout << "Sum of elements which are less than 10: " << sumLessThanTen << std::endl;

	// Task 2 ///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string sentence = "Lorem ipsum dolor sit amet, unum oporteat electram at vim. Minimum expetenda vis ad, simul scripta volutpat ei vim, id sint veniam usu. His ex stet delicata, mel ne unum.";

	// Making string vector from sentence.
	sentence.erase(std::remove(sentence.begin(), sentence.end(), ','), sentence.end());		// Remove ','
	sentence.erase(std::remove(sentence.begin(), sentence.end(), '.'), sentence.end());		// Remove '.'
	std::vector<std::string> sentenceVector = split(sentence, ' ');							// Make vector by string split by ' '

	/*3*/
	std::string newString = "";
	auto addElememnts = [&newString] (const std::string& element) { newString += (element + " "); };
    std::for_each(sentenceVector.begin(), sentenceVector.end(), addElememnts);
	std::cout << "New concatenated string: " << newString << std::endl;

	/*4*/
	auto over3Chars = [](std::string element) {return (element.size() > 3);};
	auto moreThan3Chars = std::count_if (sentenceVector.begin(), sentenceVector.end(), over3Chars);
	std::cout << "Number of words with more than 3 characters: " << moreThan3Chars << std::endl;

	return 0;
}
