//============================================================================
// Name        : CPP-Obj-D2-T2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "headers/Rectangle.h"
#include "headers/Circle.h"
#include "headers/Shape.h"

/*
 *  Create base class for representing general shape. Create classes which derives from this class and provide methods to calculate perimeter and area. Make base class pure.
    Do the same as above, but using pointers: each calculation of perimeter/area should be done on the same pointer.
    Do the same, but object should be created with new()
 */

int main() {
	Rectangle r1(5,2);
	Circle c1(5);
	Shape * shapePtr;

	shapePtr = &r1;
	std::cout << "Rec area: " << shapePtr->calcArea() << std::endl;
	std::cout << "Rec perm: " << shapePtr->calcPerimeter() << std::endl;

	shapePtr = &c1;
	std::cout << "Circle area: " << shapePtr->calcArea() << std::endl;
	std::cout << "Circle perm: " << shapePtr->calcPerimeter() << std::endl;


	shapePtr = new Rectangle(8,4);
	std::cout << "Rec area: " << shapePtr->calcArea() << std::endl;
	std::cout << "Rec perm: " << shapePtr->calcPerimeter() << std::endl;
	delete shapePtr;
	shapePtr = nullptr;

	shapePtr = new Circle(8);
	std::cout << "Rec area: " << shapePtr->calcArea() << std::endl;
	std::cout << "Rec perm: " << shapePtr->calcPerimeter() << std::endl;
	delete shapePtr;
	shapePtr = nullptr;

	return 0;
}
