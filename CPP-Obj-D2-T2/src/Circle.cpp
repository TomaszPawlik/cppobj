/*
 * Triangle.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Circle.h"
#include <math.h>

Circle::Circle(double aRadius) :
mRadius(aRadius)
{
}

double Circle::calcArea()
{
	return M_PI*mRadius*mRadius;
}

double Circle::calcPerimeter()
{
	return 2*M_PI*mRadius;
}
