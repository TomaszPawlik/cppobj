/*
 * Rectangle.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Rectangle.h"

Rectangle::Rectangle(double aWidth, double aHeight) :
mWidth(aWidth), mHeight(aHeight)
{
}

double Rectangle::calcArea()
{
	return mWidth*mHeight;
}

double Rectangle::calcPerimeter()
{
	return (mWidth+mHeight)*2;
}

