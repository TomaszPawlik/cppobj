/*
 * Rectangle.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Shape.h"

class Rectangle: public Shape
{
public:
	Rectangle(double aWidth, double aHeight);
	virtual ~Rectangle() = default;
	double calcArea();
	double calcPerimeter();
private:
	double mWidth;
	double mHeight;
};

#endif /* RECTANGLE_H_ */
