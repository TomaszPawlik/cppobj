/*
 * Triangle.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Shape.h"

class Circle: public Shape
{
public:
	Circle(double aRadius);
	virtual ~Circle() = default;
	double calcArea();
	double calcPerimeter();
private:
	double mRadius;
};


#endif /* TRIANGLE_H_ */
