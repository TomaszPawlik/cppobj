/*
 * Shape.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef SHAPE_H_
#define SHAPE_H_

class Shape
{
public:
	Shape() = default;
	virtual ~Shape() = default;
	virtual double calcArea() = 0;
	virtual double calcPerimeter() = 0;
};

#endif /* SHAPE_H_ */
