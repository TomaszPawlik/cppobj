//============================================================================
// Name        : CPP-Obj-D3-T1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/*
 * Day 3 - Task 1 - Exceptions
 * Use function std::stoi() to convert string to number.
 * Find in the documentation, how this function will behave for differently formatted strings with numbers.
 * What will happen if std::stoi is not able to parse string? Propose exception handling for such situation.
 * Add text file to commit with console output which shows your experiments.
 */

#include <iostream>
#include <string>
#include <limits>

int main()
{
	int intVar;
	std::string stringVar = "123666666666666666666666666666666666666666666666666";

	try
	{
		intVar = std::stoi(stringVar);
	}
	catch (const std::invalid_argument& e)
	{
		std::cout << "invalid_argument exception in " << e.what() << std::endl;
	}
	catch (const std::out_of_range& e)
	{
		std::cout << "out_of_range exception in " << e.what() << ". Int is between: " << std::numeric_limits<int>::max() << " to "
				  << std::numeric_limits<int>::min() << std::endl;
	}

	std::cout << "Int from initial string: " << intVar;

	return 0;
}
