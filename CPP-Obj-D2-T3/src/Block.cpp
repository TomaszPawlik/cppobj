/*
 * Block.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Block.h"

Block::Block(std::array<double, 4> &aParamTab) :
mWidth(aParamTab[0]),
mHeight(aParamTab[1]),
mLength(aParamTab[2]),
mWeight(aParamTab[3])
{
}

double Block::getWidth() const
{
	return mWidth;
}

double Block::getHeight() const
{
	return mHeight;
}

double Block::getLength() const
{
	return mLength;
}

double Block::getWeight() const
{
	return mWeight;
}

double Block::countVolume()
{
	return mWidth*mLength*mHeight;
}

double Block::countSurfaceArea()
{
	return 2*(mWidth*mHeight)+2*(mWidth*mLength)+2*(mHeight*mLength);
}

double Block::countDensity()
{
	return mWeight/countVolume();
}
