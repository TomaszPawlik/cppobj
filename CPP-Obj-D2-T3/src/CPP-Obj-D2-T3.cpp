//============================================================================
// Name        : CPP-Obj-D2-T3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "headers/Block.h"
#include "headers/Sphere.h"
#include <array>

/*
    - Write a class Block that creates a block. The constructor should take an array as an argument, this will contain 4 integers of the form [width, length, height, mass] from which the Block should be created.
      Define methods to return width, height, lenght, volume, surface area, density.
    - Write a class Sphere that creates a sphere. The constructor should take an array as an argument, this will contain 2 integers of the form [radius,mass] from which the Sphere should be created.
      Define methods to return radius, mass, volumen, surface area, density.
    - Create base class which will provide common interface for both Sphere and Block
 */

int main()
{
	constexpr int blockParameters = 4;
	std::array<double, blockParameters> arrayForBlock {4,2,3,100};

	constexpr int sphereParameters = 2;
	std::array<double, sphereParameters> arrayForSphere {4,40};

	Block blockObj(arrayForBlock);
	Sphere sphereObj(arrayForSphere);

	InterfaceFor3DShapes * shapesPtr = &blockObj;
	std::cout << shapesPtr->countDensity() << std::endl;
	shapesPtr = &sphereObj;
	std::cout << shapesPtr->countDensity() << std::endl;

	return 0;
}
