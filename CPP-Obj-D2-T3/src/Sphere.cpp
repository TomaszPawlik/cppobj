/*
 * Sphere.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#include "headers/Sphere.h"
#include <math.h>

Sphere::Sphere(std::array<double, 2> &aParamTab) :
mRadius(aParamTab[0]),
mWeight(aParamTab[1])
{
}

double Sphere::getRadius() const
{
	return mRadius;
}

double Sphere::getWeight() const
{
	return mWeight;
}

double Sphere::countVolume()
{
	return 4/3*M_PI*mRadius*mRadius*mRadius;
}

double Sphere::countSurfaceArea()
{
	return 4*M_PI*mRadius*mRadius;
}

double Sphere::countDensity()
{
	return mWeight/countVolume();
}
