/*
 * Sphere.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef SPHERE_H_
#define SPHERE_H_

#include "InterfaceFor3DShapes.h"
#include <array>

class Sphere : public InterfaceFor3DShapes
{
public:
	Sphere(std::array<double, 2> &aParamTab);
	virtual ~Sphere() = default;
	double getRadius() const;
	double getWeight() const override;
	virtual double countVolume() override;
	virtual double countSurfaceArea() override;
	virtual double countDensity() override;
private:
	double mRadius;
	double mWeight;
};

#endif /* SPHERE_H_ */
