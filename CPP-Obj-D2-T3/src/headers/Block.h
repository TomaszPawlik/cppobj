/*
 * Block.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include "InterfaceFor3DShapes.h"
#include <array>

class Block : public InterfaceFor3DShapes
{
public:
	Block(std::array<double, 4> &aParamTab);
	virtual ~Block() = default;
	double getWidth() const;
	double getHeight() const;
	double getLength() const;
	double getWeight() const override;
	virtual double countVolume() override;
	virtual double countSurfaceArea() override;
	virtual double countDensity() override;
private:
	double mWidth;
	double mHeight;
	double mLength;
	double mWeight;
};

#endif /* BLOCK_H_ */
