/*
 * InterfaceFor3DShapes.h
 *
 *  Created on: Mar 19, 2019
 *      Author: tpawlik
 */

#ifndef INTERFACEFOR3DSHAPES_H_
#define INTERFACEFOR3DSHAPES_H_

class InterfaceFor3DShapes
{
public:
	virtual ~InterfaceFor3DShapes() = default;
	virtual double countVolume() = 0;
	virtual double countSurfaceArea() = 0;
	virtual double countDensity() = 0;
	virtual double getWeight() const = 0;
};

#endif /* INTERFACEFOR3DSHAPES_H_ */
